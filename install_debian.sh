#!/bin/bash

set -x

HERE=`pwd`
export CODE=$HERE/..
if [ "$EUID" -eq 0 ]
then
    export PREFIX=/usr
else
    export PREFIX=$HOME/.local
fi

echo $CODE
echo $PREFIX
mkdir -p $PREFIX

echo "--------Installing system dependencies--------------"
if [ "$EUID" -ne 0 ]
then
    echo 'sudo required to install system dependencies'
    sudo apt update -yq
    sudo apt install -yq --no-install-recommends \
        capnproto clang make pkg-config nettle-dev \
        libssl-dev libsqlite3-dev libssl1.1 openssl
    sudo apt install systemd
    sudo apt clean
else
    apt update -yq
    apt install -yq --no-install-recommends \
        capnproto clang make pkg-config nettle-dev \
        libssl-dev libsqlite3-dev libssl1.1 openssl
    apt install systemd
    apt clean
fi

make build
make install
make clean
