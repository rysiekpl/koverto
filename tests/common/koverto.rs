use super::{log_level, stdout};
use koverto_lib::{constants::CONFIG_FILE, tests_common::fixture};
use rand::Rng;
use std::io::prelude::*;
use std::{ffi, fs, iter, panic, path, process};

/// An koverto environment.
///
/// Creates a TempDir as home and seeds it.
/// Koverto processes can be started with `run(&[subcommand])`.
/// All processes will be killed when the struct goes out of scope.
pub struct Koverto {
    procs: Vec<process::Child>,
    send_to: Option<u16>,
    pub home: tempfile::TempDir,
    pub listen_on: u16,
}

impl Koverto {
    pub fn new(send_to: Option<u16>) -> Self {
        let koverto = Self {
            procs: vec![],
            send_to,
            home: fixture::tmp_home().unwrap(),
            listen_on: rand::thread_rng().gen_range(25000, 25999),
        };
        koverto.write_config();
        koverto
    }

    /// Start the main koverto process
    pub fn start(self: &mut Self) {
        self.run(iter::empty::<&str>());
    }

    /// Run an koverto subcommand in the background
    pub fn run<I, S>(self: &mut Self, args: I)
    where
        I: IntoIterator<Item = S>,
        S: AsRef<ffi::OsStr>,
    {
        self.procs.push(
            process::Command::new("target/debug/koverto")
                .args(&["--log-level", log_level()])
                .args(args)
                .env("HOME", self.home.path())
                .stdout(stdout())
                .spawn()
                .unwrap(),
        );
    }

    /// Execute an koverto subcommand and wait for it to finish
    pub fn exec<I, S>(self: &mut Self, args: I)
    where
        I: IntoIterator<Item = S>,
        S: AsRef<ffi::OsStr>,
    {
        let cmd = process::Command::new("target/debug/koverto")
            .args(&["--log-level", log_level()])
            .args(args)
            .env("HOME", self.home.path())
            .stdout(stdout())
            .output()
            .unwrap();
        assert!(cmd.status.success(), "{:?}", cmd);
    }

    /// Configure koverto in home to use port for sending mails.
    ///
    /// Configuration will be appended to home/.umschlaged/
    fn write_config(self: &Self) {
        let path = self.home.path().join(CONFIG_FILE);
        // The config file used to live in one dir up the rest of the
        // koverto home data.
        // The way the file is created won't overwrite an existing file.
        // XXX: i could not find other way to create or overwrite a file, so
        // delete it first. Note this not checking the case where the path is
        // a dir
        if path.is_file() {
            let _ = fs::remove_file(&path);
        }
        let mut file = fs::OpenOptions::new()
            .create(true)
            .write(true)
            .open(&path)
            .unwrap();
        write!(&mut file, "\n[receive_mta]\nport = {}\n", self.listen_on)
            .expect("failed to write config");
        if let Some(port) = self.send_to {
            write!(&mut file, "\n[send_mta]\nport = {}\n", port)
                .expect("failed to write config");
        }
        // Because our samotop branch writes the Emails in `.mail`, set this
        // directory to it instead of the default.
        write!(&mut file, "\n[[clients]]\ndirectory = \".mail\"\n")
            .expect("failed to write config");
    }

    /// Queue dir of this koverto instance.
    pub fn queue(self: &Self) -> path::PathBuf {
        // Because the Samotop branch we use puts the Email in .mail
        self.home.path().join(".mail")
    }
}

impl Drop for Koverto {
    fn drop(&mut self) {
        for proc in self.procs.iter_mut() {
            proc.kill().unwrap();
        }
    }
}
