# Makefile for a source distribution of koverto.
#
# This package is not self-contained and the build products may require other
# dependencies to function; it is given as a reference for distro packagers.

PACKAGE = koverto
VERSION = $(shell sh version.sh)
DESTDIR =

THISFILE = $(lastword $(MAKEFILE_LIST))
CARGO = cargo

# GNU command variables
# see http://www.gnu.org/prep/standards/html_node/Command-Variables.html

INSTALL = install
INSTALL_DATA = $(INSTALL) -m 644
INSTALL_PROGRAM = $(INSTALL)
INSTALL_SCRIPT = $(INSTALL)

# GNU directory variables
# see http://www.gnu.org/prep/standards/html_node/Directory-Variables.html

prefix = /usr/local
exec_prefix = $(prefix)
bindir = $(exec_prefix)/bin

datarootdir = $(prefix)/share
datadir = $(datarootdir)
sysconfdir = $(prefix)/etc

docdir = $(datarootdir)/doc/$(PACKAGE)
mandir = $(datarootdir)/man
man1dir = $(mandir)/man1
man5dir = $(mandir)/man5

# for systemd
tmpfilesdir=/usr/lib/tmpfiles.d
systemunitdir=/lib/systemd/system
# for systemd udev
networkdir=/lib/systemd/network

# for apparmor
apparmordir=/etc/apparmor.d

srcdir = .

SRC_MAN1 = man/koverto.1
SRC_MAN5 = man/koverto.toml.5
SRC_SCRIPT = target/release/koverto
SRC_DOC = README.md LICENSE.txt
SRC_UNITFILE = systemd/system/koverto.service systemd/system/koverto_mta.service
SRC_APPARMOR = apparmor.d/bin.koverto
SRC_ALL = $(SRC_SCRIPT) $(SRC_DOC) $(SRC_MAN1) $(SRC_MAN5)

DST_MAN1 = $(SRC_MAN1)
DST_MAN5 = $(SRC_MAN5)
DST_SCRIPT = $(SRC_SCRIPT)
DST_DOC = $(SRC_DOC)
DST_TMPFILES = $(SRC_TMPFILES)
DST_UNITFILE = systemd/system
DST_APPARMOR = $(SRC_APPARMOR)
DST_ALL = $(DST_SCRIPT) $(DST_DOC) $(DST_MAN1) $(DST_MAN5)

doc:
	# if doc/book.toml have build.build-dir set to "../public", there is no
	# need to pass the dest-dir argument
	mdbook build doc --dest-dir ../public
	$(CARGO) doc --release --no-deps && mkdir -p public && mv target/doc public/api

build:
	$(CARGO) build --release

all: doc build install

install:
	@echo $@

	mkdir -p $(DESTDIR)$(bindir)
	for i in $(DST_SCRIPT); do $(INSTALL_SCRIPT) "$$i" $(DESTDIR)$(bindir); done
	mkdir -p $(DESTDIR)$(docdir)
	for i in $(DST_DOC); do $(INSTALL_DATA) "$$i" $(DESTDIR)$(docdir); done
	mkdir -p $(DESTDIR)$(man1dir)
	for i in $(DST_MAN1); do $(INSTALL_DATA) "$$i" $(DESTDIR)$(man1dir); done
	mkdir -p $(DESTDIR)$(man5dir)
	for i in $(DST_MAN5); do $(INSTALL_DATA) "$$i" $(DESTDIR)$(man5dir); done

	if [ -n "$(WITH_SYSTEMD)" ]; then \
		/usr/sbin/adduser --system koverto; \
		mkdir -p $(DESTDIR)$(systemunitdir); \
		for i in $(DST_UNITFILE); do $(INSTALL_DATA) "$$i" $(DESTDIR)$(systemunitdir); done; \
		/bin/systemctl start koverto.service; \
		/bin/systemctl start koverto_mta.service; \
		/bin/systemctl status koverto.service; \
		/bin/systemctl status koverto_mta.service; \
	fi

	if [ -n "$(WITH_APPARMOR)" ]; then \
		mkdir -p $(DESTDIR)$(apparmordir); \
		for i in $(DST_APPARMOR); do $(INSTALL_DATA) "$$i" $(DESTDIR)$(apparmordir); done; \
		for i in $(DST_APPARMOR); do aa-complain $(DESTDIR)$(apparmordir)/"$$i"; done; \
	fi

uninstall:
	@echo $@
	for i in $(notdir $(DST_SCRIPT)); do rm -f $(DESTDIR)$(bindir)/"$$i"; done
	for i in $(notdir $(DST_DOC)); do rm -f $(DESTDIR)$(docdir)/"$$i"; done
	for i in $(notdir $(DST_MAN1)); do rm -f $(DESTDIR)$(man1dir)/"$$i"; done
	for i in $(notdir $(DST_MAN5)); do rm -f $(DESTDIR)$(man5dir)/"$$i"; done

	if [ -n "$(WITH_SYSTEMD)" ]; then \
		/bin/systemctl stop koverto.service; \
		/bin/systemctl stop koverto_mta.service; \
		/bin/systemctl status koverto.service; \
		/bin/systemctl status koverto_mta.service; \
	fi
	for i in $(notdir $(DST_UNITFILE)); do rm -f $(DESTDIR)$(systemunitdir)/"$$i"; done
	for i in $(notdir $(DST_APPARMOR)); do rm -f $(DESTDIR)$(apparmordir)/"$$i"; done

clean:
	$(CARGO) clean

distclean: clean

maintainer-clean: distclean
	rm -f $(DST_MAN1)
	rm -f $(DST_MAN5)

.PHONY: doc build all install uninstall clean distclean maintainer-clean
