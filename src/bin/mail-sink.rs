//! This is a tiny tool to receive and store mails
//! We use it in tests for now.
//!
//! It can also serve as a mini MTA
//! that will store the mails koverto then processes.
//! However it is by no means ready for production.

use failure::{format_err, Error};
use mail_sink::logger;
use mail_sink::opts;
use mail_sink::server;
use nix::unistd;
use privdrop::PrivDrop;
use std::env;

fn main() -> Result<(), Error> {
    let args: Vec<String> = env::args().collect();
    let opts = opts::Opts::new(&args)?;
    if opts.help_present() {
        let brief = format!("Usage: {} [options]", &args[0]);
        print!("{}", opts.opts.usage(&brief));
        return Ok(());
    }

    let server = server::from_opts(&opts)?;

    // Drop privileges if root
    if unistd::geteuid().is_root() {
        let mut privdrop = PrivDrop::default().user(opts.user());
        if let Some(group) = opts.group() {
            privdrop = privdrop.group(group);
        }
        privdrop.apply()?;
    }

    logger::setup(opts.log_directory())?;

    server.serve().map_err(|e| format_err!("{}", e))
}
