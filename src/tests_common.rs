//! Common functions for unit and integration tests.
//!
//! They are public so that they can be used in the integration tests outside
//! the crate.

pub mod fixture {
    use crate::constants::DATA_DIR;
    use crate::{config, email, openpgp};
    use failure::Fallible;
    use std::{fs, path};

    pub const TESTS_DATA_DIR: &str = "tests/data";
    pub const TESTS_HOME_DIR: &str = "tests/data/home_koverto";

    // Returns tests/data/home_koverto
    pub fn tests_home_dir() -> path::PathBuf {
        path::PathBuf::from(TESTS_HOME_DIR)
    }

    // Returns tests/data/home_koverto/X
    pub fn tests_home_join(name: &str) -> path::PathBuf {
        tests_home_dir().join(name)
    }

    // Returns tests/data/
    pub fn tests_data_dir() -> path::PathBuf {
        path::PathBuf::from(TESTS_DATA_DIR)
    }

    // Returns tests/data/X
    pub fn tests_data_join(name: &str) -> path::PathBuf {
        tests_data_dir().join(name)
    }

    // Returns /tmp/
    fn tmp() -> Fallible<tempfile::TempDir> {
        let dst_home = tempfile::tempdir()?;
        Ok(dst_home)
    }

    /// Create a TempDir that can be used as home for koverto
    ///
    /// Seeds the dir with the content of tests/data/home_koverto.
    /// Namely:
    ///  * the test keyring,
    ///  * secret key,
    ///  * tls cert fixtures,
    ///  * the configuration file
    ///
    /// NOTE: The configuration file used to live one dir up and some tests
    /// need to create their own configuration file.
    pub fn tmp_home() -> Fallible<tempfile::TempDir> {
        let dst_home = tmp()?;
        let dst_data = dst_home.path().join(DATA_DIR);
        fs::create_dir_all(&dst_data)?;
        // println!("data to copy {:?}", dst_data);
        for entry in fs::read_dir(TESTS_HOME_DIR)? {
            let file_path = entry.unwrap().path();
            let file_dst = dst_data.join(&file_path.file_name().unwrap());
            fs::copy(&file_path, &file_dst)?;
        }
        Ok(dst_home)
    }

    /// As `tmp_home`, but it also loads the contents of the keyring into the
    /// store.
    pub fn tmp_home_with_keys() -> Fallible<tempfile::TempDir> {
        let home = tmp_home()?;
        let cfg = config::Config::from(&home);
        let keys_store = openpgp::Store::new(&cfg.store_config)?;
        keys_store.import_keyring(cfg.keyring_path())?;
        assert!(keys_store.obtain_keys("user@localhost").is_ok());
        Ok(home)
    }

    pub fn incoming_email() -> Fallible<email::Incoming> {
        let email_string =
            fs::read_to_string("tests/data/email_plain_single.eml")?;
        email::Incoming::new(email_string)
    }
}

pub mod assert {
    use failure::Fallible;
    pub fn err_message<T>(res: Fallible<T>, message: &str) {
        match res {
            Ok(_) => panic!("\nExpected error did not happen."),
            Err(err) => assert_eq!(format!("{}", err), message),
        }
    }
}
