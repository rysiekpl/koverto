use crate::config::CryptoAction;
use crate::constants::{CLIENT_FROM, EMAIL_DIR};
use serde::{Deserialize, Serialize};
use std::ops::Deref;
use std::{fmt, path};

/// A client is an application that sends email to koverto.
#[derive(Serialize, Deserialize, Debug, PartialEq, Default, Clone)]
pub struct Client {
    // name: String,
    // #[serde(default = CLIENT_FROM.to_owned())]
    /// Identifies a client which sends Emails with the same `from` header.
    #[serde(default)]
    pub directory: DirectoryWrapper,
    #[serde(default)]
    pub from: ClientFromWrapper,
    // credentials: Credentials,
    // are_keys_verified: AreKeysVerified,
    /// The actions to perform on Emails received from this client.
    #[serde(default)]
    pub crypto_action: CryptoAction,
    /// Whether to only send Emails when encryption was successfull for all
    /// the recipients.
    #[serde(default)]
    pub send_all_or_none: bool,
}

impl<S> From<S> for Client
where
    S: AsRef<str>,
{
    fn from(sender: S) -> Self {
        let mut client = Client::default();
        client.from = ClientFromWrapper(sender.as_ref().to_string());
        client
    }
}

impl fmt::Display for Client {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", serde_json::to_string_pretty(&self).unwrap())
    }
}

/// `String` wrapper used in
/// [Client.from](struct.Client.html#structfield.from)
#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct ClientFromWrapper(pub String);

impl Default for ClientFromWrapper {
    fn default() -> Self {
        ClientFromWrapper(CLIENT_FROM.to_owned())
    }
}

impl Deref for ClientFromWrapper {
    type Target = String;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl AsRef<str> for ClientFromWrapper {
    fn as_ref(&self) -> &str {
        &self.0.as_str()
    }
}

impl PartialEq<str> for ClientFromWrapper {
    fn eq(&self, other: &str) -> bool {
        other.eq(&self.0)
    }
}

impl PartialEq<ClientFromWrapper> for str {
    fn eq(&self, other: &ClientFromWrapper) -> bool {
        other.0.eq(&self)
    }
}

/// `String` wrapper used in
/// [Client.directory](struct.Client.html#structfield.directory)
#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub struct DirectoryWrapper(pub String);

impl Default for DirectoryWrapper {
    fn default() -> Self {
        DirectoryWrapper(EMAIL_DIR.to_owned())
    }
}

impl Deref for DirectoryWrapper {
    type Target = String;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl AsRef<path::Path> for DirectoryWrapper {
    fn as_ref(&self) -> &path::Path {
        &self.0.as_ref()
    }
}

impl PartialEq<str> for DirectoryWrapper {
    fn eq(&self, other: &str) -> bool {
        other.eq(&self.0)
    }
}

impl PartialEq<DirectoryWrapper> for str {
    fn eq(&self, other: &DirectoryWrapper) -> bool {
        other.0.eq(&self)
    }
}
