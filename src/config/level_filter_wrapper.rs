use log;
use serde::{Deserialize, Serialize};
use std::fmt;

use crate::constants::LOG_LEVEL;

/// [log::LevelFilter](https://docs.rs/log/0.4.1/log/enum.LevelFilter.html)
/// wrapper used in [Config.log_level](struct.Config.html#structfield.log_level)
#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct LevelFilterWrapper(pub log::LevelFilter);

impl Default for LevelFilterWrapper {
    fn default() -> Self {
        LevelFilterWrapper(LOG_LEVEL)
    }
}

impl PartialEq<log::LevelFilter> for LevelFilterWrapper {
    fn eq(&self, other: &log::LevelFilter) -> bool {
        other.eq(&self.0)
    }
}

impl PartialEq<LevelFilterWrapper> for log::LevelFilter {
    fn eq(&self, other: &LevelFilterWrapper) -> bool {
        other.0.eq(self)
    }
}

impl fmt::Display for LevelFilterWrapper {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let printable = format!("{}", self.0).to_lowercase();
        write!(f, "{}", printable)
    }
}

impl From<&str> for LevelFilterWrapper {
    fn from(level: &str) -> Self {
        LevelFilterWrapper(match level.to_lowercase().as_str() {
            "trace" => log::LevelFilter::Trace,
            "debug" => log::LevelFilter::Debug,
            "info" => log::LevelFilter::Info,
            "warn" => log::LevelFilter::Warn,
            "error" => log::LevelFilter::Error,
            "off" => log::LevelFilter::Off,
            _ => panic!("Not a level filter."),
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn level_filter_from_str() {
        let lfw = LevelFilterWrapper::from("trace");
        assert_eq!(lfw, log::LevelFilter::Trace);
    }

    #[test]
    #[should_panic]
    fn level_filter_from_empty_str() {
        LevelFilterWrapper::from("");
    }

    #[test]
    fn level_filter_display() {
        let lfw = LevelFilterWrapper::from("trace");
        assert_eq!("trace", lfw.to_string());
    }
}
