use serde::{Deserialize, Serialize};
use std::fmt;
use std::ops::Deref;
use std::path;

use crate::config::paths::{data_path_from_home, default_data_dir};
use crate::constants::{STORE_FILE, STORE_NAME, STORE_REALM};

/// Configuration for the Sequoia keys store.
///
/// `default()` initializes a realm and name but home will be an empty str.
/// This is intentional to assign at run time a home
/// relative to Config home_path.
///
/// TODO: explain what realm and name are.
#[derive(Serialize, Deserialize, Debug, PartialEq, Default)]
pub struct StoreConfig {
    /// The home should be different to other [`Store`] in order to only have
    /// one [`NetworkPolicy`], the one of this `realm` and `name`.
    /// Otherwise the `Store` will use each `realm` and `name` `NetworkPolicy`,
    /// what might cause (not-guessed) network requests. [#337].
    ///
    /// [`Store`]: https://docs.sequoia-pgp.org/sequoia_store/struct.Store.html
    /// [`NetworkPolicy`]: https://docs.sequoia-pgp.org/sequoia_core/enum.NetworkPolicy.html
    /// [#337]: https://gitlab.com/sequoia-pgp/sequoia/issues/337
    #[serde(default = "default_store_path")]
    pub home: path::PathBuf,
    #[serde(default)]
    pub realm: StoreRealmWrapper,
    #[serde(default)]
    pub name: StoreNameWrapper,
    // #[serde(default = None)]
    // pub policy: Option<sequoia_core::NetworkPolicy>
}

impl<P> From<P> for StoreConfig
where
    P: AsRef<path::Path>,
{
    fn from(user_home: P) -> Self {
        StoreConfig {
            home: store_path_from_home(user_home),
            realm: StoreRealmWrapper::default(),
            name: StoreNameWrapper::default(),
        }
    }
}

impl fmt::Display for StoreConfig {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", serde_json::to_string_pretty(&self).unwrap())
    }
}

/// `String` wrapper used in
/// [StoreConfig.realm](struct.StoreConfig.html#structfield.realm)
#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct StoreRealmWrapper(String);

impl Default for StoreRealmWrapper {
    fn default() -> Self {
        StoreRealmWrapper(STORE_REALM.to_owned())
    }
}

impl Deref for StoreRealmWrapper {
    type Target = String;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

/// `String` wrapper used in
/// [StoreConfig.name](struct.StoreConfig.html#structfield.name)
#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct StoreNameWrapper(String);

impl Default for StoreNameWrapper {
    fn default() -> Self {
        StoreNameWrapper(STORE_NAME.to_owned())
    }
}

impl Deref for StoreNameWrapper {
    type Target = String;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

pub fn default_store_path() -> path::PathBuf {
    default_data_dir().join(STORE_FILE)
}

pub fn store_path_from_home<P: AsRef<path::Path>>(
    home_path: P,
) -> path::PathBuf {
    data_path_from_home(home_path).join(STORE_FILE)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tests_common::fixture;

    #[test]
    fn store_config_from() {
        let store_cfg = StoreConfig::from(fixture::tests_home_dir());
        let expected_cfg = StoreConfig {
            home: store_path_from_home(fixture::tests_home_dir()),
            realm: StoreRealmWrapper(STORE_REALM.to_owned()),
            name: StoreNameWrapper(STORE_NAME.to_owned()),
        };
        assert_eq!(store_cfg, expected_cfg);
    }

    #[test]
    fn store_config_default() {
        let store_cfg = StoreConfig::default();
        let expected_cfg = StoreConfig {
            home: path::PathBuf::from(""),
            realm: StoreRealmWrapper(STORE_REALM.to_owned()),
            name: StoreNameWrapper(STORE_NAME.to_owned()),
        };
        assert_eq!(store_cfg, expected_cfg);
    }
}
