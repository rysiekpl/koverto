//! Deserialize email stored with FileTransprot into lettre::SendableEmail

use crate::email::Prepare;
use failure::Fallible;
use lettre;
use serde::{Deserialize, Serialize};
use serde_json;

pub struct LoadOutgoing {}

#[derive(PartialEq, Eq, Clone, Debug, Serialize, Deserialize)]
struct SerializableEmail {
    envelope: lettre::Envelope,
    message_id: String,
    message: Vec<u8>,
}

impl Prepare for LoadOutgoing {
    /// Process the incoming email.
    ///
    /// It logs the errors and still returns them.
    ///
    fn prepare(&self, email: String) -> Fallible<Vec<lettre::SendableEmail>> {
        let source: SerializableEmail = serde_json::from_str(&email)?;
        let sendable = lettre::SendableEmail::new(
            source.envelope,
            source.message_id,
            source.message,
        );
        Ok(vec![sendable])
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use failure::Fallible;
    use lettre::Transport;
    use lettre::{EmailAddress, Envelope, SendableEmail};
    use std::fs;
    use tempfile;

    #[test]
    fn write_to_file_and_prepare() -> Fallible<()> {
        let temp = tempfile::tempdir()?;
        let mut transport = lettre::FileTransport::new(temp.path());
        let result = transport.send(sendable_email());
        assert!(result.is_ok());
        let email_string = fs::read_to_string(temp.path().join("id.json"))?;
        let load_outgoing = LoadOutgoing {};
        let result = load_outgoing.prepare(email_string);
        let email = result?.pop().unwrap();
        assert_eq!("id", email.message_id().to_string());
        assert_eq!("Hello world", email.message_to_string()?);
        Ok(())
    }

    fn sendable_email() -> lettre::SendableEmail {
        SendableEmail::new(
            Envelope::new(
                Some(EmailAddress::new("user@localhost".to_string()).unwrap()),
                vec![EmailAddress::new("root@localhost".to_string()).unwrap()],
            )
            .unwrap(),
            "id".to_string(),
            "Hello world".to_string().into_bytes(),
        )
    }
}
