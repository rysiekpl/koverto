use crate::{constants, email};
use failure::Fallible;
use lettre_email;

/// Build a sendable email for recipient
/// based on the incoming email.
///
/// From [RFC2822]:
///
/// ```text
/// The only required header fields are the origination date field and
/// the originator address field(s).  All other header fields are
/// syntactically optional.
/// ```
///
/// [RFC2822]: https://tools.ietf.org/html/rfc2822#section-2.2
// #3: Include headers from the original email
pub fn plain(
    source: &email::Incoming,
    recipient: &str,
) -> Fallible<lettre_email::EmailBuilder> {
    let date = time::strptime(&source.date(), constants::DATE_FMT)?;
    Ok(lettre_email::EmailBuilder::new()
        .from(source.from())
        .to(recipient)
        .subject(source.subject())
        .date(&date))
}
