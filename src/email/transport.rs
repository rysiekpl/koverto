//! Transports for delivering emails.
//!
//! The transport may in turn write files to a directory
//! or deliver to an SMTP server.
//!
//! Outgoing SMTP connections can be reused
//! depending on the configuration of the transport.
//!
//! # Mutability
//!
//! The smtp transport needs mutable state to count the number of emails send.
//! This way it can reset the connection after a given number of emails.
//!
//! We use interior mutability with a RefCell to allow for that.
//! This way `TransportWrapper` can remain immutable and still own the transport.

use crate::config;
use crate::email::Deliver;
use failure::Fallible;
use lettre;
use lettre::smtp;
use lettre::Transport;
use native_tls::TlsConnector;
use std::cell::RefCell;
use std::marker::PhantomData;
use std::path;

/// TransportWrapper to store the mails in the given path.
pub fn file(path: path::PathBuf) -> impl Deliver {
    TransportWrapper::new(lettre::FileTransport::new(path))
}

/// Setup an outgoing SMTP connection according to config.
///
/// Wraps a lettre::Smtp::Transport
/// that is used to deliver all the emails.
pub fn smtp(config: &config::SendMTA) -> impl Deliver {
    let tls_connector = TlsConnector::new().unwrap();
    let tls =
        lettre::ClientTlsParameters::new(config.domain.clone(), tls_connector);
    let mut client = lettre::SmtpClient::new(
        config.to_socket_addr().as_str(),
        lettre::smtp::ClientSecurity::Opportunistic(tls),
    )
    .unwrap();
    // Use the credentials if there are any
    if let Some(c) = &config.credentials {
        let credentials = smtp::authentication::Credentials::new(
            c.username.clone(),
            c.password.clone(),
        );
        client = client.credentials(credentials);
    }
    TransportWrapper::new(client.transport())
}

pub struct TransportWrapper<'a, T, R, E>
where
    T: Transport<'a, Result = Result<R, E>>,
    E: std::error::Error + Send + Sync + 'static,
{
    phantom: std::marker::PhantomData<&'a Result<R, E>>,
    transport: RefCell<T>,
}

impl<'a, T, R, E> TransportWrapper<'a, T, R, E>
where
    T: Transport<'a, Result = Result<R, E>>,
    E: std::error::Error + Send + Sync + 'static,
{
    pub fn new(transport: T) -> Self {
        Self {
            transport: RefCell::new(transport),
            phantom: PhantomData,
        }
    }

    /// Send a single Email.
    pub fn send_email(
        &self,
        sendable_email: lettre::SendableEmail,
    ) -> Fallible<()> {
        match self.transport.borrow_mut().send(sendable_email) {
            Ok(_) => Ok(()),
            Err(e) => Err(e.into()),
        }
    }

    fn sendable_description(
        &self,
        sendable: &lettre::SendableEmail,
    ) -> String {
        let to = &sendable.envelope().to()[0];
        let from = &sendable
            .envelope()
            .from()
            .map_or("missing sender".to_string(), |to| to.to_string());
        format!("email from {} to {}", from, to)
    }
}

impl<'a, T, R, E> Deliver for TransportWrapper<'a, T, R, E>
where
    T: Transport<'a, Result = Result<R, E>>,
    E: std::error::Error + Send + Sync + 'static,
{
    /// Send a vector of Emails.
    fn deliver(&self, sendable_emails: Vec<lettre::SendableEmail>) {
        for email in sendable_emails {
            let description = self.sendable_description(&email);
            match self.send_email(email) {
                Ok(_) => debug!("Successfully send {}.", description),
                Err(e) => error!("Error sending {}. {}", description, e),
            }
        }
    }
}
