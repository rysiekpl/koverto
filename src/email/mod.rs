//! Functions to parse/build Emails and MIME.

use failure::Fallible;

pub mod builder;
mod error;
mod incoming;
mod load_outgoing;
mod queue;
pub mod transport;
mod wrap;

pub use error::Error;
pub use incoming::Incoming;
pub use load_outgoing::LoadOutgoing;
pub use queue::Queue;
pub use wrap::Wrap;

/// Turn emails read from a Queue into sendable emails
///
pub trait Prepare {
    /// prepare SendableEmail from a queued email.
    fn prepare(&self, email: String) -> Fallible<Vec<lettre::SendableEmail>>;
}

/// Deliver emails to their destination
///
/// Destination can be a remote SMTP server or a local directory.
///
/// Implementations can be found in email::transport.
pub trait Deliver {
    /// Deliver a vector of Emails.
    fn deliver(&self, sendable_emails: Vec<lettre::SendableEmail>);
}
