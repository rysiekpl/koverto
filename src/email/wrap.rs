//! Turn an incoming email into signed and possibly encrypted emails.
//!
//! Creates one signed (and evtl. encrypted) email per recipient.

use crate::email::{Error, Prepare};
use crate::{config, email, openpgp};
use failure::{Fallible, ResultExt};

pub struct Wrap {
    crypto_action: config::CryptoAction,
    send_all_or_none: bool,
    from: config::ClientFromWrapper,
    openpgp: openpgp::OpenPGP,
}

impl Wrap {
    pub fn new(
        cfg: &config::Config,
        client: &config::Client,
    ) -> Fallible<Self> {
        let crypto_action = client.crypto_action.clone();
        debug!("Selected action: {:?}", crypto_action);
        let openpgp = openpgp::OpenPGP::new(cfg, client)?;
        Ok(Self {
            send_all_or_none: client.send_all_or_none,
            from: client.from.to_owned(),
            crypto_action,
            openpgp,
        })
    }
}

impl Prepare for Wrap {
    /// Process the incoming email and prepare the sendable emails.
    ///
    /// It logs the errors and still returns them.
    ///
    fn prepare(&self, email: String) -> Fallible<Vec<lettre::SendableEmail>> {
        let incoming = email::Incoming::new(email)?;
        if incoming.from() != &self.from {
            return Err(Error::NoAction.into());
        }
        if self.send_all_or_none {
            self.all_or_none(incoming)
                .with_context(|e| {
                    format!(
                    "No email sent cause at least one recipient failed: {}", e)
                })
                .map_err(|e| e.into())
        } else {
            Ok(self.available_emails(incoming))
        }
    }
}

impl Wrap {
    fn all_or_none(
        &self,
        incoming: email::Incoming,
    ) -> Fallible<Vec<lettre::SendableEmail>> {
        incoming
            .recipients()
            .into_iter()
            .map(|recipient| self.build_with_context(&incoming, &recipient))
            .collect()
    }

    fn available_emails(
        &self,
        incoming: email::Incoming,
    ) -> Vec<lettre::SendableEmail> {
        incoming
            .recipients()
            .into_iter()
            .map(|recipient| self.build_with_context(&incoming, &recipient))
            .filter_map(|build_result| match build_result {
                Ok(sendable_email) => {
                    trace!("Successfully built email.");
                    Some(sendable_email)
                }
                Err(e) => {
                    warn!("{}", e);
                    None
                }
            })
            .collect()
    }

    /// Constructs an outgoing email for the given recipient.
    ///
    /// Takes care of the cryptographic operations on the mail body
    /// and constructs the mime message.
    ///
    /// Picks the cryptographic actions to perform
    /// based on the selected `CryptoAction`.
    fn build_with_context(
        &self,
        incoming: &email::Incoming,
        recipient: &str,
    ) -> Fallible<lettre::SendableEmail> {
        trace!("Building email to recipient {}", &recipient);
        self.build(&incoming, &recipient)
            .with_context(|e| {
                format!("{} when composing to {}.", e, &recipient)
            })
            .map_err(|e| e.into())
    }

    fn build(
        &self,
        incoming: &email::Incoming,
        recipient: &str,
    ) -> Fallible<lettre::SendableEmail> {
        Ok(self.builder(&incoming, &recipient)?.build()?.into())
    }

    fn builder(
        &self,
        source: &email::Incoming,
        recipient: &str,
    ) -> Fallible<lettre_email::EmailBuilder> {
        match self.crypto_action {
            config::CryptoAction::MustEncrypt => {
                self.must_encrypt(&source, &recipient)
            }
            config::CryptoAction::Fallback => {
                self.fallback(&source, &recipient)
            }
            config::CryptoAction::SignOnly => {
                self.sign_only(&source, &recipient)
            }
        }
    }

    fn must_encrypt(
        &self,
        source: &email::Incoming,
        recipient: &str,
    ) -> Fallible<lettre_email::EmailBuilder> {
        let output = self.openpgp.sign_encrypt(source, recipient)?;
        let email_builder = email::builder::plain(&source, recipient)
            .with_context(|e| format!("{} {}", output, e))?;
        Ok(email_builder.openpgp_encrypted(output.content))
    }

    fn sign_only(
        &self,
        source: &email::Incoming,
        recipient: &str,
    ) -> Fallible<lettre_email::EmailBuilder> {
        let output = self.openpgp.sign(source)?;
        let email_builder = email::builder::plain(&source, recipient)
            .with_context(|e| format!("{} {}", output, e))?;
        // As of Sep 2019, Sequoia always use sha512
        let sendable = email_builder.openpgp_signed(
            source.body(),
            output.content,
            "pgp-sha512",
        );
        Ok(sendable)
    }

    fn fallback(
        &self,
        source: &email::Incoming,
        recipient: &str,
    ) -> Fallible<lettre_email::EmailBuilder> {
        self.must_encrypt(source, recipient)
            .or_else(|_err| self.sign_only(source, recipient))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tests_common::fixture;
    use std::fs;

    #[test]
    fn prepare_encrypt() {
        let home = fixture::tmp_home_with_keys().unwrap();
        let cfg = config::Config::from(&home);
        let client = &cfg.clients.first().expect("theres should be a client");
        let wrap = Wrap::new(&cfg, client).unwrap();
        let email = wrap.prepare(incoming_email());
        assert!(email.is_ok());
    }

    #[test]
    fn prepare_sign() {
        let home = fixture::tmp_home_with_keys().unwrap();
        let cfg = config::Config::from(&home);
        let mut client = config::Client::default();
        client.crypto_action = config::CryptoAction::SignOnly;
        let wrap = Wrap::new(&cfg, &client).unwrap();
        let email = wrap.prepare(incoming_email());
        assert!(email.is_ok());
    }

    #[test]
    fn prepare_sign_encrypt() {
        let home = fixture::tmp_home_with_keys().unwrap();
        let cfg = config::Config::from(&home);
        let mut client = config::Client::default();
        client.crypto_action = config::CryptoAction::MustEncrypt;
        // Sign and encrypt.
        let wrap = Wrap::new(&cfg, &client).unwrap();
        let email = wrap.prepare(incoming_email());
        assert!(email.is_ok());
    }

    #[test]
    fn prepare_err_with_send_all() {
        let home = fixture::tmp_home_with_keys().unwrap();
        let cfg = config::Config::from(&home);
        let mut client = config::Client::default();
        client.send_all_or_none = true;
        // Encrypt.
        let wrap = Wrap::new(&cfg, &client).unwrap();
        let email = wrap.prepare(incoming_email());
        assert!(email.is_err());
        let expected = "No email sent cause at least one recipient failed: \
                        Failed to sign and encrypt. No usable encryption key: \
                        Key not found when composing to foo@localhost.";
        assert_eq!(expected, format!("{}", email.err().unwrap()));
    }

    fn incoming_email() -> String {
        fs::read_to_string("tests/data/email_plain.eml").unwrap()
    }
}
