//! Constants used by different modules
//! They set the application defaults.

use log;

pub const LOG_LEVEL: log::LevelFilter = log::LevelFilter::Info;
pub const LOG_MODULE: &str = "koverto_lib";
pub const CONFIG_FILE: &str = "koverto.toml";
pub const DATA_DIR: &str = "";
pub const STORE_FILE: &str = "store";
pub const STORE_REALM: &str = "org.sequoia-pgp.koverto";
pub const STORE_NAME: &str = "default";
pub const RECEIVE_MTA_ADDRESS: &str = "127.0.0.1";
pub const RECEIVE_MTA_PORT: u32 = 25465;
pub const RECEIVE_MTA_TLS_CERT_FILE: &str = "koverto.pfx";
pub const CLIENT_NAME: &str = "application";
// TODO: use other type and/or implement conversions
pub const CLIENT_FROM: &str = "application@localhost.localdomain";
pub const SEND_MTA_DOMAIN: &str = "127.0.0.1";
pub const SEND_MTA_PORT: u32 = 465;
pub const KEYRING_FILE: &str = "keyring";
pub const KEY_FILE_EXTENSION: &str = ".pgp";
pub const SECRET_KEY_SUFFIX: &str = "_secret";
pub const DATE_FMT: &str = "%a, %d %b %Y %H:%M:%S %z";
// Extension of the Email files
pub const EMAIL_EXT: &str = ".eml";
// Directory in which the Email files are stored.
pub const EMAIL_DIR: &str = "mail";
pub const OUTGOING_EMAIL_DIR: &str = "outgoing";
