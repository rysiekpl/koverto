//! Toplevel module for Koverto.
//!
//! Brings together the different pieces
//! and implements `run()` which is called from main.
//!
//! `run()` will parse the command line args,
//! read the configuration,
//! setup logging,
//! and then either handle subcommands or
//! run an koverto process that waits for incoming mail.

// option.as_deref() does not exist in rust < 1.40
#![allow(clippy::option_as_ref_deref)]

#[macro_use]
extern crate clap;
#[macro_use]
extern crate log;
#[macro_use]
extern crate failure;
#[macro_use]
extern crate prettytable;

use failure::Fallible;

pub mod cli;
pub mod config;
pub mod constants;
pub mod email;
pub mod openpgp;
// To be used in unit and integration tests
pub mod tests_common;

/// Main function that returns errors.
pub fn run() -> Fallible<()> {
    // We still need to load the config and configure the logger.
    // It does not seem possible to change the log level
    // after initializing the logger with `log` and `pretty-env-logger`.
    // So we use `println` here because we cannot predict the log level.
    // Issue #52 addresses logging to a file and might also tackle this.
    println!("📧🔑 Starting koverto...");

    // Create cli arguments parser.
    let opt = cli::parse();

    // Parse config argument.
    let config_path = match opt.config.clone() {
        Some(path_str) => path_str,
        None => config::default_config_path(),
    };

    // Load configuration
    let cfg = match config::Config::load(&config_path) {
        Err(error) => {
            if opt.config.is_some() {
                let error_message = format_err!(
                    "Error loading configuration {}: {}.",
                    config_path.to_str().unwrap(),
                    error
                );
                return Err(error_message);
            } else {
                println!("Using configuration defaults");
                config::Config::new()
            }
        }
        Ok(cfg) => {
            println!(
                "Using configuration file {}.",
                config_path.to_str().unwrap()
            );
            cfg
        }
    };
    println!("Configuration: {}", toml::to_string(&cfg).unwrap());

    // Once we have a way to adjust the log level
    // we should start with a default log level above (see comment)
    // and change it here rather than setting it.
    let log_level = config::LevelFilterWrapper::from(
        opt.log_level
            .unwrap_or_else(|| cfg.log_level.to_string())
            .as_str(),
    );

    pretty_env_logger::formatted_builder()
        .filter(Some(&cfg.log_module), log_level.0)
        // Because samotop does not return errors, log them.
        .filter(Some("samotop"), log::LevelFilter::Warn)
        .init();

    match opt.subcommand {
        Some(subcmd) => cli::subcommands::handle(subcmd, &cfg),
        None => watch_queue(std::sync::Arc::new(cfg)),
    }
}

fn watch_queue(cfg_arc: std::sync::Arc<config::Config>) -> Fallible<()> {
    let cfg = cfg_arc.clone();
    std::thread::spawn(move || {
        let client = cfg
            .clients
            .first()
            .expect("There must be at least one client - or the default one.");
        let sign_and_encrypt = sign_and_encrypt_to_outbox(&cfg, &client)
            .map_err(|err| {
                error!("Failed to watch queue dir: {}", err);
                err
            })
            .unwrap();
        match sign_and_encrypt.watch() {
            Err(err) => error!("Processing emails failed: {}.", err),
            Ok(()) => error!("Email prepare thread ended unexpectedly."),
        }
    });
    let deliver_to_smtp = outbox_to_smtp(&cfg_arc).map_err(|err| {
        error!("Failed to watch queue dir: {}", err);
        err
    })?;
    deliver_to_smtp.watch()
}

fn sign_and_encrypt_to_outbox(
    cfg: &config::Config,
    client: &config::Client,
) -> Fallible<email::Queue> {
    let wrap = email::Wrap::new(&cfg, &client)?;
    let prepare = Box::new(wrap);
    let transport = email::transport::file(cfg.outgoing_email_path());
    let sender = Box::new(transport);
    let path = &cfg.home_path.join(&client.directory);
    Ok(email::Queue::new(path, prepare, sender))
}

fn outbox_to_smtp(cfg: &config::Config) -> Fallible<email::Queue> {
    let load_outgoing = email::LoadOutgoing {};
    let prepare = Box::new(load_outgoing);
    let transport = email::transport::smtp(&cfg.send_mta);
    let sender = Box::new(transport);
    let path = &cfg.outgoing_email_path();
    Ok(email::Queue::new(path, prepare, sender))
}
