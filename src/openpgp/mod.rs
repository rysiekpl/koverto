//! OpenPGP functions (encrypt, sign, armor) using
//! [sequoia_openpgp](https://docs.sequoia-pgp.org/0.9.0/sequoia_openpgp/index.html).
//!
//! If/when `Sequoia` will implement a high-level API, this module might not be
//! needed.
//! Until them, some of the code is partial copy/paste from
//! [Sequoia guides](https://docs.sequoia-pgp.org/sequoia_guide/)
//! and examples.

use sequoia_openpgp;
use sequoia_openpgp::armor;
use sequoia_openpgp::packet::key;
use sequoia_openpgp::packet::prelude::*;
use sequoia_openpgp::serialize::stream::*;
use sequoia_openpgp::types::{DataFormat, KeyFlags};
// For as_tsk().serialize()?
use sequoia_openpgp::serialize::Serialize;

use std::io;
// Needed for .write_all()
use failure::{Fallible, ResultExt};
use std::io::Write;
use std::{fmt, fs};

use super::{config, email};

mod store;

pub use store::Store;

/// A wrapper around the cryptographic data
///
/// Serves as a high level api that we can define ourselves.
/// An OpenPGP instance has access to the cryptographic data
/// needed by a particular client.
///
/// Processes incoming emails
/// and returns bodies or signatures
/// for composing the outgoing mails.
///
/// Does not handle the construction of Mime messages.
pub struct OpenPGP {
    secret_key: sequoia_openpgp::Cert,
    keys_store: store::Store,
}

impl OpenPGP {
    pub fn new(
        config: &config::Config,
        client: &config::Client,
    ) -> Fallible<Self> {
        let secret_key = store::obtain_secret_key(&client.from, &config)?;
        let keys_store = store::Store::new(&config.store_config)?;
        Ok(Self {
            secret_key,
            keys_store,
        })
    }

    pub fn sign_encrypt(
        &self,
        source: &email::Incoming,
        recipient: &str,
    ) -> Fallible<Output> {
        let output = sign_encrypt(
            &source,
            &self.keys_store,
            recipient,
            &self.secret_key,
        )
        .with_context(|e| format!("Failed to sign and encrypt. {}", e))?;
        Ok(output)
    }

    pub fn sign(&self, source: &email::Incoming) -> Fallible<Output> {
        let output = sign_only(&source.body(), &self.secret_key)
            .with_context(|e| format!("Failed to sign. {}", e))?;
        Ok(output)
    }

    pub fn encrypt(
        &self,
        source: &email::Incoming,
        recipient: &str,
    ) -> Fallible<Output> {
        let output = encrypt_only(&source, &self.keys_store, recipient)
            .with_context(|e| format!("Failed to encrypt. {}", e))?;
        Ok(output)
    }
}

pub struct Output {
    pub content: String,
    pub encryption_key: Option<String>,
    pub signing_key: Option<String>,
}

impl Output {
    pub fn new(
        content: String,
        encryption_key: Option<String>,
        signing_key: Option<String>,
    ) -> Self {
        Self {
            content,
            encryption_key,
            signing_key,
        }
    }

    pub fn signing_key_str(&self) -> &str {
        self.signing_key
            .as_ref()
            .map(|string| string.as_str())
            .unwrap_or("none")
    }

    pub fn encryption_key_str(&self) -> &str {
        self.encryption_key
            .as_ref()
            .map(|string| string.as_str())
            .unwrap_or("none")
    }
}

impl fmt::Display for Output {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "with signing key: {} and encryption key: {}",
            self.signing_key_str(),
            self.encryption_key_str(),
        )
    }
}

/// Inline sign, encrypt and enarmor a text.
pub fn sign_encrypt(
    email: &email::Incoming,
    keys_store: &store::Store,
    recipient: &str,
    secret_key: &sequoia_openpgp::Cert,
) -> Fallible<Output> {
    let signing_key = secret_key.keyid().to_string();

    // Sign the email body with the sender.
    let mut signed_message = Vec::new();
    sign_inline(&mut signed_message, &email.body(), &secret_key)
        .with_context(|e| format!("Could not sign: {}", e))?;

    let cert = keys_store
        .obtain_keys(&recipient)
        .with_context(|e| format!("No usable encryption key: {}", e))?;
    let keyid = cert.keyid().to_string();

    // Encrypt the email body with the recipients and TPKs
    let mut cipherbytes = vec![];
    encrypt(&mut cipherbytes, &signed_message, vec![cert])?;
    // trace!("{:?}", cipherbytes);

    // Enarmor the message
    let mut enarmored = vec![];
    enarmor_message(&mut enarmored, &cipherbytes)?;
    let new_body = String::from_utf8_lossy(&enarmored).to_string();
    trace!("body: {}", new_body);
    Ok(Output::new(new_body, Some(keyid), Some(signing_key)))
}

/// Sign only a text for the given sender.
pub fn sign_only<S>(
    body: S,
    secret_key: &sequoia_openpgp::Cert,
) -> Fallible<Output>
where
    S: AsRef<str>,
{
    let signing_key = secret_key.keyid().to_string();

    // Sign the email body with the sender.
    let mut signed_message = Vec::new();
    sign_detached(&mut signed_message, body, &secret_key)?;

    // Enarmor the signature
    let mut detached_sign = Vec::new();
    enarmor_signature(&mut detached_sign, &signed_message)?;
    let signature = String::from_utf8_lossy(&detached_sign).to_string();
    trace!("signature: {}", signature);
    Ok(Output::new(signature, None, Some(signing_key)))
}

/// Only encrypt a text for the given recipients.
pub fn encrypt_only(
    email: &email::Incoming,
    keys_store: &store::Store,
    recipient: &str,
) -> Fallible<Output> {
    let cert = keys_store
        .obtain_keys(&recipient)
        .with_context(|e| format!("No usable key: {}", e))?;
    let keyid = cert.keyid().to_string();

    // Encrypt the email body with the recipients and TPKs
    let mut ciphertext = vec![];
    encrypt_str(&mut ciphertext, email.body(), vec![cert])
        .with_context(|e| format!("generating ciphertext - {}", e))?;

    // Enarmor the ciphertext
    let mut buffer = vec![];
    enarmor_message(&mut buffer, &ciphertext)
        .with_context(|e| format!("enarmoring ciphertext - {}", e))?;
    let new_body = String::from_utf8_lossy(buffer.as_ref()).to_string();
    trace!("body: {}", new_body);
    Ok(Output::new(new_body, Some(keyid), None))
}

/// Encrypt a text for transport with the recipents' public keys.
fn encrypt_str<S>(
    sink: &mut dyn io::Write,
    plaintext: S,
    certs: Vec<sequoia_openpgp::Cert>,
) -> Fallible<()>
where
    S: AsRef<str>,
{
    encrypt(sink, plaintext.as_ref().as_bytes(), certs)
}

/// Encrypt bytes for transport with the recipents' public keys.
fn encrypt(
    sink: &mut dyn io::Write,
    plainbytes: &[u8],
    certs: Vec<sequoia_openpgp::Cert>,
) -> Fallible<()> {
    trace!("Encrypting ciphertext...");

    // XXX: Invalid operation: Key XXX has no suitable encryption subkey

    // Start streaming an OpenPGP message.
    let message = Message::new(sink);
    let mut recipients = recipients_from_certs(&certs);
    let first_recipient = recipients.pop().expect(
        "We make sure there's at least one recipient when parsing the mail.",
    );
    // Encrypt a literal data packet.
    let encryptor = recipients.into_iter().fold(
        Encryptor::for_recipient(message, first_recipient),
        |e, r| e.add_recipient(r),
    );
    let encryptor = encryptor.build()?;

    // Emit a literal data packet.
    let mut literal_writer = LiteralWriter::new(encryptor)
        .format(DataFormat::Binary)
        .build()?;

    // Encrypt the data.
    literal_writer.write_all(plainbytes)?;

    // Finalize the OpenPGP message to make sure that all data is
    // written.
    literal_writer.finalize()?;

    trace!("Successfully encrypted plaintext.");

    Ok(())
}

fn recipients_from_certs(certs: &[sequoia_openpgp::Cert]) -> Vec<Recipient> {
    certs
        .iter()
        .flat_map(|cert| encryption_keys_from_cert(&cert))
        .map(|key| key.into())
        .collect()
}

fn encryption_keys_from_cert(
    cert: &sequoia_openpgp::Cert,
) -> Vec<&Key<key::PublicParts, key::UnspecifiedRole>> {
    cert.keys()
        .policy(None)
        .alive()
        .revoked(false)
        .key_flags(
            KeyFlags::default()
                .set_storage_encryption(true)
                .set_transport_encryption(true),
        )
        .map(|amalgamation| amalgamation.key())
        .collect()
}

fn create_file<S>(file_path: S) -> Fallible<Box<dyn io::Write>>
where
    S: AsRef<str>,
{
    Ok(Box::new(
        fs::OpenOptions::new()
            .write(true)
            .truncate(true)
            .create(true)
            .open(file_path.as_ref())?,
    ))
}

/// Generates a signing-capable key and stores it in the file system
///
/// TODO: store it in private sequoia store when it is implemented.
pub fn generate_signing_key<S>(
    sender: S,
    cfg: &config::Config,
) -> Fallible<sequoia_openpgp::Cert>
where
    S: AsRef<str>,
{
    let sender = sender.as_ref();
    let (cert, revocation) = sequoia_openpgp::cert::CertBuilder::new()
        .add_userid(sender)
        .add_signing_subkey()
        .generate()?;

    // Save the key
    let cert_path = cfg.secret_key_path(sender);
    trace!("cert path {}", cert_path.to_string_lossy());
    let w = create_file(&cert_path.to_str().unwrap())?;
    let mut w = armor::Writer::new(w, armor::Kind::SecretKey, &[])?;
    cert.as_tsk().serialize(&mut w)?;

    // Save the revocation certificate.
    let rev_path = cfg.data_path().join(format!("{}.rev", sender));
    trace!("rev path {}", rev_path.to_string_lossy());
    let w = create_file(&rev_path.to_str().unwrap())?;
    let mut w = armor::Writer::new(w, armor::Kind::Signature, &[])?;
    sequoia_openpgp::Packet::Signature(revocation).serialize(&mut w)?;
    Ok(cert)
}

/// Detached sign a text with a secret key.
fn sign_detached<S>(
    sink: &mut dyn io::Write,
    text: S,
    tsk: &sequoia_openpgp::Cert,
) -> Fallible<()>
where
    S: AsRef<str>,
{
    trace!("Detached signing plaintext...");

    let key = signing_key(tsk);
    let keypair = key.into_keypair()?;

    let message = Message::new(sink);
    let mut signer = Signer::new(message, keypair).detached().build()?;
    signer.write_all(text.as_ref().as_bytes())?;
    signer.finalize()?;
    trace!("Successfully signed planintext.");
    Ok(())
}

/// Sign a text inline with a secret key.
fn sign_inline<S>(
    sink: &mut dyn io::Write,
    text: S,
    tsk: &sequoia_openpgp::Cert,
) -> Fallible<()>
where
    S: AsRef<str>,
{
    trace!("Signing inline...");

    let key = signing_key(tsk);
    let keypair = key.into_keypair()?;

    let message = Message::new(sink);
    let signer = Signer::new(message, keypair).build()?;
    let mut ls = LiteralWriter::new(signer)
        .format(DataFormat::Text)
        .build()?;

    ls.write_all(text.as_ref().as_bytes())?;
    ls.finalize()?;
    trace!("Successfully signed inline.");
    Ok(())
}

fn signing_key(
    tsk: &sequoia_openpgp::Cert,
) -> Key<key::SecretParts, key::UnspecifiedRole> {
    tsk.keys()
        .policy(None)
        .alive()
        .revoked(false)
        .for_signing()
        .secret()
        .next()
        .unwrap()
        .key()
        .clone()
}

/// Enarmor signature bytes.
fn enarmor_signature(
    sink: &mut dyn io::Write,
    input_data: &[u8],
) -> Fallible<()> {
    trace!("Enarmoring signature...");
    {
        let armor_kind = armor::Kind::Signature;
        let mut writer = armor::Writer::new(sink, armor_kind, &[])?;
        writer.write_all(&input_data)?;
        writer.finalize()?;
    }
    trace!("Successfully enarmored signature.");
    Ok(())
}

/// Enarmor message bytes.
fn enarmor_message(
    sink: &mut dyn io::Write,
    input_data: &[u8],
) -> Fallible<()> {
    trace!("Enarmoring message...");
    {
        let armor_kind = armor::Kind::Message;
        let mut writer = armor::Writer::new(sink, armor_kind, &[])?;
        writer.write_all(&input_data)?;
        writer.finalize()?;
    }
    trace!("Successfully enarmored message.");
    Ok(())
}

#[cfg(test)]
mod tests {

    use std::collections::HashMap;
    // for .read_to_string();
    use sequoia_openpgp::crypto::{KeyPair, SessionKey};
    use sequoia_openpgp::parse::stream::*;
    use sequoia_openpgp::parse::Parse;
    use sequoia_openpgp::types::SymmetricAlgorithm;
    use std::io::Read;

    use super::*;
    use crate::tests_common::fixture;

    const MESSAGE: &str = "Test body.\n";

    fn dearmor(
        sink: &mut dyn Read,
    ) -> Fallible<(Vec<u8>, Option<armor::Kind>)> {
        let mut reader = armor::Reader::new(sink, None);
        let mut dearmored = Vec::<u8>::new();
        reader.read_to_end(&mut dearmored)?;
        Ok((dearmored, reader.kind()))
    }

    struct Helper<'a>(&'a sequoia_openpgp::Cert);

    impl<'a> VerificationHelper for Helper<'a> {
        fn get_public_keys(
            &mut self,
            _: &[sequoia_openpgp::KeyHandle],
        ) -> Fallible<Vec<sequoia_openpgp::Cert>> {
            Ok(vec![self.0.clone()])
        }
        fn check(&mut self, structure: MessageStructure) -> Fallible<()> {
            if let MessageLayer::SignatureGroup { ref results } =
                structure.iter().next().unwrap()
            {
                if let VerificationResult::GoodChecksum { .. } =
                    results.get(0).unwrap()
                {
                    Ok(()) /* good */
                } else {
                    panic!()
                }
            } else {
                panic!()
            }
        }
    }

    /// This helper provides secrets for the decryption, fetches public
    /// keys for the signature verification and implements the
    /// verification policy.
    struct HHelper {
        keys: HashMap<sequoia_openpgp::KeyID, KeyPair>,
    }

    impl HHelper {
        /// Creates a HHelper for the given TPKs with appropriate secrets.
        fn new(certs: Vec<sequoia_openpgp::Cert>) -> Self {
            HHelper {
                keys: certs_to_hash_map(certs),
            }
        }
    }

    fn certs_to_hash_map(
        certs: Vec<sequoia_openpgp::Cert>,
    ) -> HashMap<sequoia_openpgp::KeyID, KeyPair> {
        certs
            .iter()
            .flat_map(|cert| encryption_keys_from_cert(cert))
            .map(|key| {
                key.clone()
                    .mark_parts_secret()
                    .expect("no secret key found")
            })
            .map(|key| {
                (
                    key.keyid(),
                    key.into_keypair()
                        .expect("could not turn key into keypair"),
                )
            })
            .collect()
    }

    impl DecryptionHelper for HHelper {
        fn decrypt<D>(
            &mut self,
            pkesks: &[sequoia_openpgp::packet::PKESK],
            _skesks: &[sequoia_openpgp::packet::SKESK],
            mut decrypt: D,
        ) -> Fallible<Option<sequoia_openpgp::Fingerprint>>
        where
            D: FnMut(SymmetricAlgorithm, &SessionKey) -> Fallible<()>,
        {
            // Try each PKESK until we succeed.
            for pkesk in pkesks {
                if let Some(pair) = self.keys.get_mut(pkesk.recipient()) {
                    let res =
                        pkesk.decrypt(pair).and_then(|(algo, session_key)| {
                            decrypt(algo, &session_key)
                        });
                    if res.is_ok() {
                        break;
                    }
                }
            }
            // XXX: In production code, return the Fingerprint of the
            // recipient's TPK here
            Ok(None)
        }
    }

    impl VerificationHelper for HHelper {
        fn get_public_keys(
            &mut self,
            _ids: &[sequoia_openpgp::KeyHandle],
        ) -> failure::Fallible<Vec<sequoia_openpgp::Cert>> {
            Ok(Vec::new()) // Feed the TPKs to the verifier here.
        }
        fn check(&mut self, _: MessageStructure) -> Fallible<()> {
            Ok(()) // Implement your verification policy here.
        }
    }

    /// Decrypts the given message.
    fn decrypt(
        sink: &mut dyn io::Write,
        ciphertext: &[u8],
        recipient: sequoia_openpgp::Cert,
    ) -> Fallible<()> {
        // Make a helper that that feeds the recipient's secret key to the
        // decryptor.
        let helper = HHelper::new(vec![recipient]);

        // Now, create a decryptor with a helper using the given TPKs.
        let mut decryptor = Decryptor::from_bytes(ciphertext, helper, None)?;

        // Decrypt the data.
        io::copy(&mut decryptor, sink)?;

        Ok(())
    }

    #[test]
    fn decrypt_encrypted() {
        let cert = user_secret_key();
        let certs = vec![cert.clone()];

        // Encrypt the message
        let mut cipherbytes = vec![];
        encrypt_str(&mut cipherbytes, MESSAGE, certs).unwrap();

        // Decrypt the message.
        let mut plainbytes = Vec::new();
        decrypt(&mut plainbytes, &cipherbytes, cert).unwrap();
        let plainstring = String::from_utf8_lossy(&plainbytes);

        assert_eq!(plainstring, MESSAGE);
    }

    #[test]
    fn decrypt_file() {
        let cert = user_secret_key();
        // Open the armored encrypted message
        let mut email_body_encrypted_enarmored =
            fs::File::open("tests/data/body_encrypted.asc").unwrap();
        // Dearmor the message
        let (cipherbytes, kind) =
            dearmor(&mut email_body_encrypted_enarmored).unwrap();
        assert_eq!(kind, Some(armor::Kind::Message));

        // Decrypt the message.
        let mut plainbytes = Vec::new();
        decrypt(&mut plainbytes, &cipherbytes, cert).unwrap();
        let plainstring = String::from_utf8_lossy(&plainbytes);

        assert_eq!(plainstring, MESSAGE);
    }

    #[test]
    fn decrypt_verify_encrypted_signed() {
        // Get signing key
        let key = server_secret_key();

        // Sign the message.
        let mut signed_message = Vec::new();
        sign_inline(&mut signed_message, MESSAGE, &key).unwrap();
        // println!("{:?}", signed_message);

        let cert = user_secret_key();
        let certs = vec![cert.clone()];

        // Encrypt the message
        let mut cipherbytes = vec![];
        encrypt(&mut cipherbytes, &signed_message, certs).unwrap();
        // println!("{:?}", cipherbytes);

        // Enarmor the message
        let mut enarmored = vec![];
        enarmor_message(&mut enarmored, &cipherbytes).unwrap();

        // let enarmored_string = String::from_utf8_lossy(&enarmored);
        // println!("{}", enarmored_string);

        // Dearmor de message
        // let (dearmored, _kind) = dearmor(&mut enarmored).unwrap();

        // Decrypt the message.
        let mut plainbytes = Vec::new();
        decrypt(&mut plainbytes, &cipherbytes, cert).unwrap();

        // Verify the signature
        let mut verifier =
            Verifier::from_bytes(&plainbytes, Helper(&key), None).unwrap();

        let mut message = String::new();
        verifier.read_to_string(&mut message).unwrap();
        assert_eq!(&message, MESSAGE);
    }

    #[test]
    fn decrypt_verify_file() {
        // Open the armored encrypted message
        let mut email_body_signed_encrypted_enarmored = fs::File::open(
            "tests/data/email_signed_encrypted_from_app_to_user.eml",
        )
        .unwrap();

        // Dearmor the message
        let (cipherbytes, _kind) =
            dearmor(&mut email_body_signed_encrypted_enarmored).unwrap();

        // Decrypt the message.
        let cert = user_secret_key();
        let mut plainbytes = Vec::new();
        decrypt(&mut plainbytes, &cipherbytes, cert).unwrap();
        // let plainstring = String::from_utf8_lossy(&plainbytes);
        // println!("{:?}", plainbytes);

        // Get signing key
        let key = server_secret_key();
        // Verify the signature
        let mut verifier =
            Verifier::from_bytes(&plainbytes, Helper(&key), None).unwrap();

        let mut message = String::new();
        verifier.read_to_string(&mut message).unwrap();
        assert_eq!(&message, MESSAGE);
    }

    #[test]
    fn dearmor_file() {
        let mut f = fs::File::open("tests/data/body_signature.asc").unwrap();
        let (_dearmored, kind) = dearmor(&mut f).unwrap();
        assert_eq!(kind, Some(armor::Kind::Signature));
    }

    #[test]
    fn verify_detached_signature() {
        let key = server_secret_key();

        // Sign the message.
        let mut signed_message = Vec::new();
        sign_detached(&mut signed_message, MESSAGE, &key).unwrap();
        // println!("detached signed message {:?}", signed_message);

        // Enarmor the signature
        let mut detached_sign = Vec::new();
        enarmor_signature(&mut detached_sign, &signed_message).unwrap();

        // Verify the signature
        let mut verifier = DetachedVerifier::from_bytes(
            &detached_sign,
            MESSAGE.as_bytes(),
            Helper(&key),
            None,
        )
        .unwrap();

        let mut message = String::new();
        verifier.read_to_string(&mut message).unwrap();
        assert_eq!(&message, MESSAGE);
    }

    #[test]
    fn verify_inline_signature() {
        let key = server_secret_key();

        let mut signed_message = Vec::new();
        sign_inline(&mut signed_message, MESSAGE, &key).unwrap();

        let mut verifier =
            Verifier::from_bytes(&signed_message, Helper(&key), None).unwrap();

        let mut message = String::new();
        verifier.read_to_string(&mut message).unwrap();
        assert_eq!(&message, MESSAGE);
    }

    #[test]
    fn encrypting_only() {
        let home = fixture::tmp_home_with_keys().unwrap();
        let cfg = config::Config::from(&home);
        let incoming = incoming_email();
        let keys_store = store::Store::new(&cfg.store_config).unwrap();
        keys_store.import_keyring(cfg.keyring_path()).unwrap();
        let output =
            encrypt_only(&incoming, &keys_store, "user@localhost").unwrap();
        assert_eq!(
            output.encryption_key,
            Some("2547 06A7 DF3D BE88".to_owned())
        );
    }

    fn incoming_email() -> email::Incoming {
        let file = fixture::tests_data_join("email_plain_single.eml");
        let string = fs::read_to_string(&file).unwrap();
        email::Incoming::new(string).unwrap()
    }

    fn user_secret_key() -> sequoia_openpgp::Cert {
        let pile = sequoia_openpgp::PacketPile::from_file(
            fixture::tests_home_join("user@localhost_secret.pgp"),
        )
        .unwrap();
        sequoia_openpgp::Cert::from_packet_pile(pile).unwrap()
    }

    fn server_secret_key() -> sequoia_openpgp::Cert {
        let pile =
            sequoia_openpgp::PacketPile::from_file(fixture::tests_home_join(
                "application@localhost.localdomain_secret.pgp",
            ))
            .unwrap();
        sequoia_openpgp::Cert::from_packet_pile(pile).unwrap()
    }
}
