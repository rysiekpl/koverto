//! Functions to manage a `Sequoia`
//! [`Store`](https://docs.sequoia-pgp.org/0.9.0/sequoia_store/struct.Store.html)
//!
//! If/when `Sequoia` will implement a high-level API, this module might not be
//! needed.
//! Until them, some of the code is partial copy/paste from `Sequoia` `sq`
//!tool.

use prettytable::{Cell, Row, Table};
use sequoia_core;
use sequoia_openpgp::parse::Parse;
use sequoia_store;

use failure::Fallible;
use std::path;

use crate::{config, openpgp};

#[derive(Debug)]
pub struct Store {
    store: sequoia_store::Mapping,
}

impl Store {
    /// Open and/or creates the `Sequoia Store`.
    pub fn new(store_config: &config::StoreConfig) -> Fallible<Self> {
        let ctx = sequoia_core::Context::configure()
            .home(&store_config.home)
            // TODO: add config variable for policy
            .network_policy(sequoia_core::NetworkPolicy::Offline)
            .build()?;
        let store = sequoia_store::Mapping::open(
            &ctx,
            &store_config.realm,
            &store_config.name,
        )?;
        Ok(Store { store })
    }

    /// List the label-fingerprint of the keys found in the `Store`.
    pub fn list_bindings(&self) -> Fallible<()> {
        let mut table = Table::new();
        table.set_format(
            *prettytable::format::consts::FORMAT_NO_LINESEP_WITH_TITLE,
        );
        table.set_titles(row!["label", "fingerprint"]);
        for (label, fingerprint, _) in self.store.iter()? {
            table.add_row(Row::new(vec![
                Cell::new(&label),
                Cell::new(&fingerprint.to_string()),
            ]));
        }
        table.printstd();
        Ok(())
    }

    /// List the keys in the `Store`.
    pub fn list_keys(&self) -> Fallible<()> {
        self.list_bindings()
    }

    /// Delete a key from the `Store` which label matches the given
    /// Email address.
    pub fn delete_key<S>(&self, email_address: S) -> Fallible<()>
    where
        S: AsRef<str>,
    {
        let email_address = email_address.as_ref();
        let binding = self.store.lookup(email_address)?;
        binding.delete()?;
        info!("Key for {} deleted.", email_address);
        Ok(())
    }

    /// Delete the `Store`.
    pub fn delete(self) -> Fallible<()> {
        self.store.delete()?;
        info!("Store deleted.");
        Ok(())
    }

    /// Import the keys in a keyring file to a `Store`.
    pub fn import_keyring<P>(&self, keyring_path: P) -> Fallible<()>
    where
        P: AsRef<path::Path>,
    {
        info!(
            "Importing keys from {}...",
            keyring_path.as_ref().to_string_lossy()
        );

        let pile =
            sequoia_openpgp::PacketPile::from_file(keyring_path.as_ref())?;
        let iter =
            sequoia_openpgp::cert::CertParser::from_iter(pile.into_children());
        let mut fps = Vec::new();
        for packets in iter {
            // Discard invalid and non-cert packets
            let cert = packets?;
            // Keep the fingerprints to show them to the user
            fps.push(cert.fingerprint().to_string());
            // Import the userids
            for uidb in cert.userids() {
                let uid = uidb.userid();
                // XXX: handle `New key conflicts with the current key` error,
                // by deleting the previous key and adding the one in the keyring.
                self.store.import(&uid.email()?.unwrap(), &cert)?;
            }
        }
        info!("Keys imported successfully: {}.", fps.join(", "));
        Ok(())
    }

    /// Obtain keys from the `Store`.
    pub fn obtain_keys(
        &self,
        recipient: &str,
    ) -> Fallible<sequoia_openpgp::Cert> {
        let cert = self.store.lookup(&recipient)?.cert()?;
        cert.alive(None)?;
        if cert.revoked(None)
            != sequoia_openpgp::RevocationStatus::NotAsFarAsWeKnow
        {
            return Err(format_err!("Key revoked."));
        }
        trace!("Found keys in the store for {}.", recipient);
        Ok(cert)
    }

    pub fn lookup(&self, fp: &str) -> Fallible<sequoia_store::Binding> {
        self.store.lookup(fp)
    }
}

/// Obtain a secret key from a file in the data directory.
///
/// TODO: If/when sequoia implements secret key store this needs to be changed.
pub fn obtain_secret_key<S>(
    sender: S,
    cfg: &config::Config,
) -> Fallible<sequoia_openpgp::Cert>
where
    S: AsRef<str>,
{
    // For now, search for a secret key in the default dir
    let secret_key_path = cfg.secret_key_path(&sender);
    trace!("Secret key path: {}", secret_key_path.to_string_lossy());
    let tsk = match sequoia_openpgp::Cert::from_file(&secret_key_path) {
        Ok(tsk) => tsk,
        Err(err) => {
            info!(
                "Could not parse secret key {}. {}",
                secret_key_path.to_string_lossy(),
                err
            );
            info!("Generating a new one...");
            openpgp::generate_signing_key(&sender, cfg)?
        }
    };
    // TODO: Use secret subkey keyid/fingerprint
    trace!("Using secret key {}", tsk.keyid().to_string());
    Ok(tsk)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{config, constants, tests_common::fixture};
    use tempfile;

    #[test]
    fn import_keyring_succeed() {
        let store_path = tempfile::TempDir::new().unwrap();
        let cfg = config::StoreConfig::from(&store_path);
        let keys_store = Store::new(&cfg).unwrap();
        let keyring_file = fixture::tests_home_join("keyring");
        keys_store.import_keyring(keyring_file).unwrap();
        let fp = keys_store
            .lookup(constants::CLIENT_FROM)
            .unwrap()
            .cert()
            .unwrap()
            .fingerprint()
            .to_string();
        assert_eq!("A751 99FA 7B1C 370A 7927  C74A 5C0F B3FA 8B82 5DA3", fp);
    }

    #[test]
    fn obtain_existing_secret_key() {
        let home = fixture::tmp_home_with_keys().unwrap();
        let cfg = config::Config::from(&home);
        let tsk = obtain_secret_key(constants::CLIENT_FROM, &cfg).unwrap();
        // TODO: Use secret subkey keyid/fingerprint
        assert_eq!("5C0F B3FA 8B82 5DA3", tsk.keyid().to_string())
    }

    #[test]
    fn obtain_secret_key_generate() {
        let home = fixture::tmp_home_with_keys();
        let cfg = config::Config::from(&home.unwrap());
        let tsk =
            obtain_secret_key("wiki@localhost.localdomain", &cfg).unwrap();
        assert!(tsk.is_tsk());
    }
}
