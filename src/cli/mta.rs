//! Receive-only MTA
use futures::Future;
use samotop;

use crate::config;

/// Run `Samotop` task to listen for incoming Emails.
pub fn run_task(
    receive_mta_config: &config::ReceiveMTA,
) -> impl Future<Item = (), Error = ()> {
    info!("Starting receive-only Mail Transfer Agent.");

    let mut tlsconf = samotop::model::controll::TlsConfig::default();
    tlsconf.id.file = receive_mta_config.tls_certificate_path.clone();

    // Mail service, use a given name or default to host name
    let mail_service = samotop::service::mail::ConsoleMail::default();

    // Tcp service
    let tcp = samotop::service::tcp::SamotopService::new(
        samotop::service::session::StatefulSessionService::new(mail_service),
        tlsconf.check_identity(),
    );

    // Build the server task
    samotop::builder()
        .with(tcp)
        .on(receive_mta_config.to_string())
        .build_task()
        // Samotop logs the errors, but don't return them, so this probably
        // won't log anything.
        .map_err(|err| error!("{:?}", err))
}
