//! Handle subcommands to the koverto command line call

use crate::{cli, config, openpgp};
use failure::Fallible;

pub fn handle(cmd: cli::SubCommand, cfg: &config::Config) -> Fallible<()> {
    // Initialize store.

    match cmd {
        cli::SubCommand::Import { keyring_path } => {
            keys_store(cfg)?.import_keyring(keyring_path)
        }
        cli::SubCommand::DeleteStore => keys_store(cfg)?.delete(),
        cli::SubCommand::DeleteKey { key } => keys_store(cfg)?.delete_key(key),
        cli::SubCommand::ListKeys => keys_store(cfg)?.list_keys(),
        cli::SubCommand::Mta => {
            // Deprecated!
            // Run the MTA that receives the emails and write them to a
            // directory.
            // This will be removed as part of #54.
            // Please use a separate MTA to queue the mails instead.
            // It might be added back when #43 will be possible
            tokio::run(cli::mta::run_task(&cfg.receive_mta));
            // This would block until the task is finished, which should be
            // never if nothing fails, therefore, return error.
            // Samotop does not return error, only log it.
            Err(failure::err_msg("Failed listening for incoming Emails."))
        }
    }
}

fn keys_store(cfg: &config::Config) -> Fallible<openpgp::Store> {
    openpgp::Store::new(&cfg.store_config)
}
