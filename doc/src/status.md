Implementation status
=====================

(January 2020)

`koverto` trys to keep things simple and implements the minimal requirements
explained in [design](design.md).

`koverto` has not been optimized for performance yet.
For now we are targetting small deployments.
We plan to improve on this once we have some usage data
and know where the bottlenecks are

In particular we are processing mails sequentially in a single thread for now.
Inotify.rs would offer [EventStream] - an async API.
This will most likely be used in future releases.
It does require an async runtime
and for now we are priorityzing a small [trusted computing base].

Dependencies limitations
-------------------------

Some limitations of koverto currently result from the status of its dependencies:

[Inotify]:

- Requires linux OS
- Does not work in a linux container on a windows host.
  Windows file systems are mounted as network volumes
  and thus do not provide inotify.

Notify.rs could be used instead.
It is currently unmaintained and we keep it simple for now.
If you want to use koverto without inotify let us know.


[Sequoia]:

- The [Store] is not thread safe [sequoia-336].
  We're currently using a single threaded approach.
  Having a store instance per thread and using a thread pool
  would be an alternative
- The Store does not support secret keys.
  Workaround reading keys from the koverto data directory.

[Samotop]:

- Save the email structure received, maybe send it to a channel [samotop-3].
  Workaround using a clone that saves the data to a directory
  (that is not configurable) [samotop-juga].

[rust-email]:

- Can only parse [RFC 822] Headers.
- Does not create [RFC 1847] (Security Multiparts for MIME) Headers
  [rust-email-50].
  Workaround using a clone [rust-email-juga].

[mime]

- Does not implement [RFC 1847] [mime-113].
  Workaround adding using a clone [mime-juga].

[lettre-email]:

- MimeMultipartType does not implement [RFC 1847].

[lettre-email]: https://docs.rs/lettre_email/0.9.2/lettre_email/
[lettre-338]: https://github.com/lettre/lettre/issues/338
[lettre-juga]: https://github.com/lettre/tree/rfc1847_dev
[lettre]: https://docs.rs/lettre/0.9.2/lettre/
[mime]: https://docs.rs/mime/0.3.14/mime/
[mime-113]: https://github.com/hyperium/mime/issues/113
[mime-juga]:  https://github.com/juga0/mime/tree/rfc1847_dev
[RFC 1847]: https://tools.ietf.org/html/rfc1847
[RFC 822]: https://tools.ietf.org/html/rfc822/
[rust-email-50]: https://github.com/niax/rust-email/issues/50
[rust-email-juga]:  https://github.com/juga0/rust-email/tree/rfc1847_dev
[rust-email]: https://docs.rs/email/0.0.20/email/
[samotop-3]: https://gitlab.com/BrightOpen/BackYard/Samotop/merge_requests/3
[samotop-juga]:  https://github.com/juga0/rust-email/tree/rfc1847_dev
[Samotop]: https://docs.rs/samotop/0.7.4/samotop/
[Sequoia]: https://docs.sequoia-pgp.org/0.9.0/
[sequoia-336]: https://gitlab.com/sequoia-pgp/sequoia/issues/336
[Store]:  https://docs.sequoia-pgp.org/0.9.0/sequoia_store/struct.Store.html

Other needed improvements
--------------------------

- Code variables name congruencies
- Tests, tests, more tests
