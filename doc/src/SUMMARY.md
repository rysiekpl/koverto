# Summary

<!--
  SUMMARY syntax does not allow to have an User and Technical section,
  that's why there must be a user.md and technical.md pages.
  Unfortunately, that implies to have to repeat the subchapter, since otherwise
  clicking on the page will not show anything.
-->

- [README](README.md)
- [User](user.md)
  - [Installation](INSTALL.md)
  - [`koverto` Manual page](koverto.1.md)
  - [Configuration manual page](koverto.toml.5.md)
- [Technical](technical.md)
  - [Analysis](analysis.md)
  - [kuvert sessions](kuvert_sessions.md)
  - [MTA servers](mta_servers.md)
  - [Samotop evaluation](samotop_evaluation.md)
  - [Samotop session](samotop_session.md)
  - [Design](design.md)
  - [Diagrams](diagrams.md)
  - [Implementation status](status.md)
  - [Contributing](CONTRIBUTING.md)
  - [Maintainers](maintainers.md)
  - [Code of Conduct](CODE_OF_CONDUCT.md)
  - [Merge requests](merge_requests.md)
  - [Code Style](code_style.md)
  - [Releases](releases.md)
