# Diagrams

## Deployment with docker

See the diagram [Docker deployment](images/deployment_impossible.svg)

## Deployment where the applications run commands

Example deployment scenarios in which `Other` and `SignOnly` cases are not
possible because the applications do not allow to configure arbitrary commands.

![Deployment Impossible](images/deployment_impossible.svg)

## Class diagram

![Classes](images/class_diagram.svg)
[SVG file](images/class_diagram.svg)

Rust does not have classes, though `struct`s can be modelled in a similar way.
